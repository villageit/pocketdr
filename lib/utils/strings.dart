class Strings {
  static const String APP_NAME = "PocketDr";
  static const String ABOUT = "About";
  static const String MY_DETAILS = "My Details";
  static const String CONNECT = "Connect";
  static const String T_AND_C = "Terms and conditions";
  static const String FB = "FaceBook";
  static const String FB_URL = "https://www.facebook.com/MedicaMedia";
  static const String TWITTER_URL = "https://mobile.twitter.com/PocketDr";
  static const String SUPPORT_EMAIL = "feedback@medicamediagroup.com";
  static const String TWITTER = "Twitter";
  static const String MAIL = "Email";
  static const String LAST_UPDATE = "last_updated";
  static const int SEVEN_DAYS_TIMESTAMP = 604800000;
}

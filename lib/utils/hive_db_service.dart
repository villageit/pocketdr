import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

Future openBox(name) async {
  var dir = await getApplicationDocumentsDirectory();
  Hive.init(dir.path);
  Box box = await Hive.openBox(name);
  return box;
}

Future putListData(name, data) async {
  Box box = await Hive.openBox(name);
  await box.clear();

  //insert data if data is list
  for (var d in data) {
    box.add(d);
  }
  return box;
}

Future getDataFromAPI(api) async {
  var responce = await http.get(Uri.parse(api));
  return jsonDecode(responce.body);
}

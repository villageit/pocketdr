
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {

  //getter and setter method string shared preferences
  static Future<String> getStringPreference(String? key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key!) ?? '';
  }
  static Future<bool> setStringPreference(String key,String? value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value!);
  }


  static Future<int> getIntPreference(String? key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key!) ?? 0;
  }

  static Future<bool> setIntPreference(String key,int? value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(key, value!);
  }


  //getter and setter method bool shared preferences
  static Future<bool> setBooleanPreference(String key,bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(key, value);
  }

  static Future<bool?> getBoolPreference(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }
}
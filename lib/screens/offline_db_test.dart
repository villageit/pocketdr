import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pocketdr/utils/api_list.dart';
import 'package:pocketdr/utils/strings.dart';
import 'package:pocketdr/utils/hive_db_service.dart' as dbService;

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class OfflineDBScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OfflineDB();
  }
}

class OfflineDB extends StatefulWidget {
  @override
  _OfflineDBState createState() => _OfflineDBState();
}

class _OfflineDBState extends State<OfflineDB> {
  late Box testBox;
  List data = [];
  Future openBox() async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    testBox = await Hive.openBox("test_data");
    return;
  }

  Future<bool> getAllData() async {
    await openBox();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var now = DateTime.now().millisecondsSinceEpoch;
    var islastUpdatedExists = prefs.containsKey(Strings.LAST_UPDATE);
    print(islastUpdatedExists);
    try {
      //Check last updated time here to fetch recent data
      if (islastUpdatedExists == false) {
        print('if');
        var decodedRes = await dbService.getDataFromAPI(APIs.GET_DATA);
        await putData(decodedRes);

        prefs.setInt(Strings.LAST_UPDATE, now);
      } else {
        print('else');
        var lastUpdated = prefs.getInt(Strings.LAST_UPDATE);
        if ((now - lastUpdated!) > Strings.SEVEN_DAYS_TIMESTAMP) {
          print('out dated');
          var decodedRes = await dbService.getDataFromAPI(APIs.GET_DATA);
          await putData(decodedRes);

          prefs.setInt(Strings.LAST_UPDATE, now);
        }
      }
    } catch (SocketException) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No internet"),
      ));
    }

    //get data from db
    var mymap = testBox.toMap().values.toList();
    if (mymap.isEmpty) {
      data.add("empty");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data"),
      ));
    } else {
      data = mymap;
    }

    return Future.value(true);
  }

  Future putData(data) async {
    await testBox.clear();

    //insert data if data is list
    for (var d in data) {
      testBox.add(d);
    }
  }

  Future<void> updateData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var now = DateTime.now().millisecondsSinceEpoch;
    try {
      var decodedRes = await dbService.getDataFromAPI(APIs.GET_DATA);
      await putData(decodedRes);
      prefs.setInt(Strings.LAST_UPDATE, now);

      setState(() {});
    } catch (SocketException) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No internet"),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Test"),
        ),
        body: listView(context),
      ),
    );
  }

  Widget listView(BuildContext context) {
    return FutureBuilder(
        future: getAllData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (data.contains('empty')) {
              return Center(child: Text("No data"));
            } else {
              return Column(
                children: [
                  Expanded(
                      child: RefreshIndicator(
                    onRefresh: updateData,
                    child: ListView.builder(
                        itemCount: data.length,
                        itemBuilder: (ctx, index) {
                          return Text(data[index]['title']);
                        }),
                  ))
                ],
              );
            }
          } else {
            return CircularProgressIndicator();
          }
        });
  }
}

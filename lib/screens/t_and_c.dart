import 'package:flutter/material.dart';
import 'package:pocketdr/utils/strings.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class TandCScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TandC();
  }
}

class TandC extends StatefulWidget {
  @override
  _TandCState createState() => _TandCState();
}

class _TandCState extends State<TandC> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Strings.T_AND_C),
        ),
        body: loadPDF(context),
      ),
    );
  }

  Widget loadPDF(BuildContext context) {
    return PDF(
      enableSwipe: true,
      swipeHorizontal: true,
      autoSpacing: false,
      pageFling: false,
      onError: (error) {
        print(error.toString());
      },
      onPageError: (page, error) {
        print('$page: ${error.toString()}');
      },
    ).cachedFromUrl('http://africau.edu/images/default/sample.pdf');
  }
}

import 'package:flutter/material.dart';
import 'package:pocketdr/utils/strings.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:mailto/mailto.dart';

class ConnectScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Connect();
  }
}

class Connect extends StatefulWidget {
  @override
  _ConnectState createState() => _ConnectState();
}

class _ConnectState extends State<Connect> {
  //Launchinhg urls
  void _launchURL(_url) async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';

  //Opening mail
  launchMailto() async {
    final mailtoLink = Mailto(to: [Strings.SUPPORT_EMAIL]);
    await launch('$mailtoLink');
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Strings.CONNECT),
        ),
        body: listView(context),
      ),
    );
  }

  Widget listView(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          leading: Icon(Icons.facebook),
          title: Text(Strings.FB),
          onTap: () {
            _launchURL(Strings.FB_URL);
          },
        ),
        ListTile(
          leading: Icon(Icons.facebook),
          title: Text(Strings.TWITTER),
          onTap: () {
            _launchURL(Strings.TWITTER_URL);
          },
        ),
        ListTile(
          leading: Icon(Icons.mail),
          title: Text(Strings.MAIL),
          onTap: () {
            launchMailto();
          },
        )
      ],
    );
  }
}

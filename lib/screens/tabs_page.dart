import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pocketdr/screens/offline_db_test.dart';
import 'package:pocketdr/utils/strings.dart';

import 'connect.dart';
import 't_and_c.dart';

class TabsScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Tabs();
  }
}

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  late FirebaseMessaging messaging;

  @override
  void initState() {
    super.initState();
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print(value);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Notification"),
              content: Text(event.notification!.body!),
              actions: [
                TextButton(
                  child: Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.home)),
              Tab(icon: Icon(Icons.article)),
              Tab(icon: Icon(Icons.list)),
              Tab(icon: Icon(Icons.tablet)),
              Tab(icon: Icon(Icons.insights))
            ],
          ),
          title: Text(Strings.APP_NAME),
          actions: [
            PopupMenuButton(
                icon: Icon(
                  Icons.more_vert,
                  color: Colors.white,
                ),
                itemBuilder: (BuildContext bc) => [
                      PopupMenuItem(child: Text(Strings.ABOUT), value: "about"),
                      PopupMenuItem(
                          child: Text(Strings.MY_DETAILS), value: "my_details"),
                      PopupMenuItem(
                          child: Text(Strings.CONNECT), value: Strings.CONNECT),
                      PopupMenuItem(child: Text("Test DB check"), value: "a")
                    ],
                onSelected: (route) async {
                  if (route == Strings.T_AND_C) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => TandCScreen()));
                  } else if (route == Strings.CONNECT) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConnectScreen()));
                  } else if (route == "a") {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OfflineDBScreen()));
                  }
                })
          ],
        ),
        body: TabBarView(
          children: [
            Icon(Icons.directions_car),
            Icon(Icons.directions_transit),
            Icon(Icons.directions_bike),
            Icon(Icons.directions_bike),
            Icon(Icons.directions_bike)
          ],
        ),
      ),
    );
  }
}
